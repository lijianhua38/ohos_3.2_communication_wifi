# Copyright (C) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/test.gni")
import("//foundation/communication/wifi/wifi/wifi.gni")

################################################################################

module_output_path = "wifi/manager_test"

config("module_private_config") {
  visibility = [ ":*" ]
  include_dirs = [
    "//commonlibrary/c_utils/base/include",
    "$WIFI_ROOT_DIR/test/wifi_standard/wifi_framework/wifi_manage/unittest",
  ]
}

ohos_unittest("manager_unittest") {
  module_out_path = module_output_path
  sources = [
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/common/wifi_broadcast_helper.cpp",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/common/wifi_datashare_utils.cpp",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/common/wifi_permission_helper.cpp",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/common/wifi_permission_utils.cpp",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/wifi_auth_center.cpp",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/wifi_config_center.cpp",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/wifi_dumper.cpp",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/wifi_internal_event_dispatcher.cpp",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/wifi_manager.cpp",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/wifi_net_agent.cpp",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/wifi_service_manager.cpp",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_toolkit/config/wifi_settings.cpp",
    "//foundation/systemabilitymgr/samgr/services/samgr/native/source/service_registry.cpp",
    "common/wifi_broadcast_helper_test.cpp",
    "common/wifi_permission_helper_test.cpp",
    "wifi_auth_center_test.cpp",
    "wifi_config_center_test.cpp",
    "wifi_dumper_test.cpp",
    "wifi_internal_event_dispatcher_test.cpp",
    "wifi_settings_test.cpp",
  ]

  include_dirs = [
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_toolkit/config",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/common",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_toolkit/include",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_toolkit/net_helper",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_toolkit/utils",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_toolkit/log",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_toolkit/config",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/wifi_sta",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/wifi_scan",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/wifi_ap",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/wifi_p2p",
    "$WIFI_ROOT_DIR/interfaces/inner_api",
    "$WIFI_ROOT_DIR/frameworks/native/interfaces",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/idl_client",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/idl_client/idl_interface",
    "$WIFI_ROOT_DIR/services/wifi_standard/ipc_framework/cRPC/include",
    "//commonlibrary/c_utils/base/include",
    "//base/hiviewdfx/hilog/interfaces/native/innerkits/include",
    "//base/notification/common_event_service/cesfwk/innerkits/include",
    "//base/notification/common_event_service/cesfwk/kits/native/include",
    "//foundation/appexecfwk/adapter/interfaces/innerkits/appexecfwk_base/include",
    "//base/security/access_token/interfaces/innerkits/accesstoken/include",
    "$WIFI_ROOT_DIR/utils/inc",
    "//foundation/systemabilitymgr/samgr/interfaces/innerkits/samgr_proxy/include",
    "//foundation/systemabilitymgr/samgr/utils/native/include",
    "$WIFI_ROOT_DIR/services/wifi_standard/include",
  ]

  deps = [
    "$WIFI_ROOT_DIR/services/wifi_standard/ipc_framework/cRPC:crpc_client",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_manage/idl_client:wifi_idl_client",
    "$WIFI_ROOT_DIR/services/wifi_standard/wifi_framework/wifi_toolkit:wifi_toolkit",
    "$WIFI_ROOT_DIR/utils:wifi_utils",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  ldflags = [
    "-fPIC",
    "--coverage",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_manager",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "data_share:datashare_common",
    "data_share:datashare_consumer",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
    "ipc:ipc_single",
    "netmanager_base:net_conn_manager_if",
    "netmanager_base:net_native_manager_if",
  ]

  defines = []
  if (wifi_feature_with_p2p) {
    defines += [ "FEATURE_P2P_SUPPORT" ]
  }

  defines += [
    "FEATURE_AP_SUPPORT",
    "AP_INSTANCE_MAX_NUM=$wifi_feature_with_ap_num",
  ]
  defines += [ "SUPPORT_RANDOM_MAC_ADDR" ]
  configs = [ ":module_private_config" ]

  # Do not modify the permission configuration of the unit test
  defines += [ "PERMISSION_ALWAYS_GRANT" ]

  part_name = "wifi"
  subsystem_name = "communication"
  testonly = true
}

group("unittest") {
  testonly = true
  deps = [ ":manager_unittest" ]
}
